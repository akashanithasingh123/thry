import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ThryService } from '../thry.service';
import { ToastrService } from 'ngx-toastr';
import { GoogleLoginProvider, SocialAuthService, SocialUser } from '@abacritt/angularx-social-login';

declare var jQuery: any;
  
@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  email: string;
  password: string;
  customer:any;
  isCaptchaResolved: boolean = false;

  isSendingOTP: boolean = false;

 currentStep: 'email' | 'otp' | 'setPassword';
  constructor(private router:Router,private toaster:ToastrService,private authService: SocialAuthService,private service :ThryService){
    this.email="";
    this.password="";
    this.currentStep = 'email';
  }

  ngOnInit() {
  }

  validateMail(mail:any):boolean{
    return /[@]/.test(mail);
  }

  handleCaptchaResolved(event: any): void {
    this.isCaptchaResolved = true;
  }

  passwordValidator(password: string): boolean {
    return  /[A-Z]/.test(password) && /[!@#$%^&*()_+{}\[\]:;<>,.?~\\-]/.test(password) &&  /[0-9]/.test(password);
  }

  async validateLogin(loginForm:any){
      await this.service.Login(loginForm).then((data: any) => {
        console.log(data);
        this.customer = data;
      });   
      if (this.customer != null) {
        this.service.setUserLoggedIn();
        this.router.navigate(['home']);      
      } else {
        this.toaster.error('Invalid Creadentials');
      }
  }

  onGoogleSignIn(googleUser: SocialUser): void {
    if (googleUser) {
      console.log('Google User Data:', googleUser);
      // You can access user data like googleUser.id, googleUser.name, etc.
      // Perform any additional actions with the user data here
    } else {
      console.error('Google Sign-In Error: No user data received');
    }
  }

 forgot(){
  jQuery('#forgot').modal('show');
 }
 submit(){
  jQuery('#forgot').modal('hide');
 }

  verifyEmail(emailForm: any) {
    this.email = emailForm.email;
    this.isSendingOTP = true;
    this.service.sendOtp(emailForm.email).subscribe(response => {
      this.isSendingOTP = false;
      if (response.message === 'OTP is sent !!!') {
        this.toaster.success("OTP Sent", "Success");
        this.currentStep = 'otp';
      } else {
        this.toaster.error("Email not Found", "Failed");
      }
      console.log(response);
    });
  }

  verifyOtp(otpForm: any) {
    this.service.verifyOtp(this.email, otpForm.otp).subscribe(response => {
      if (response.message === "OTP is valid !!!") {
        this.toaster.success("OTP Verified", "Success");
        this.currentStep = 'setPassword';
      } else {
        this.toaster.error("Invalid OTP", "Failed");
      }
      console.log(otpForm);
    });
  }


  setPassword(setPasswordForm: any) {
    this.service.setPassword(this.email, setPasswordForm.password).subscribe(response => {
      if (response.message === 'Password Updated !!!') {
        this.toaster.success("Password Updated", "Success");
        this.router.navigate(['login']);
      } else {
        this.toaster.error("Failed to Update", "Failed");
      }
      console.log(setPasswordForm);
    });
    jQuery('#forgot').modal('hide');
  }

  validateRepeat(password: string, rpassword: string): boolean {
    return password == rpassword;
  }
}


