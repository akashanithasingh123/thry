import { ActivatedRouteSnapshot, CanActivateFn, RouterStateSnapshot } from '@angular/router';
import { ThryService } from './thry.service';
import { Injectable, inject } from '@angular/core';


export const authGuard: CanActivateFn = (route, state) => {
  let service = inject(ThryService);
  return service.getUserLoggedStatus();
  // return false;
};
