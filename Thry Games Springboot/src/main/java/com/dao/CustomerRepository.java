package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,String>{

	@Query("from Customer e where e.name = :ename")
	Customer findByName(@Param("ename") String customerName);

	@Query("from Customer e where e.email = :emailId and e.password = :password")
	Customer empLogin(@Param("emailId") String emailId, @Param("password") String password);
	
	@Query("from Customer where email=:emailId")
	Customer findByEmail(@Param("emailId") String emailId);

	@Query("from Customer s where s.mobileNo = :mobileNo")
	Customer findByMobileNo(@Param("mobileNo") String phoneNo);
}
